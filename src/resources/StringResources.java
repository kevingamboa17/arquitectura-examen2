package resources;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import view.View;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by kevingamboa17 on 10/18/17.
 */
public class StringResources {
    private static final Logger bugLog = Logger.getLogger(StringResources.class);
    private static final Logger logger = Logger.getLogger(StringResources.class);
    private static final String bundlePath = "resources.i18n.MyBundle";

    private Locale mLocale;
    private ResourceBundle mResourceBundle;

    public StringResources() {
        PropertyConfigurator.configure("src/logConfiguration.properties");
        logger.info("StringResources created");
        mLocale = new Locale("es");
        mResourceBundle = ResourceBundle.getBundle(bundlePath, mLocale);
    }

    public void changeLanguaje(String newLanguage) {
        logger.debug("changeLangue for: " + newLanguage);
        this.mLocale = new Locale(newLanguage);
        mResourceBundle = ResourceBundle.getBundle(bundlePath, mLocale);
    }

    public String getString(String key) {
        logger.debug("getting string with key: " + key);
        return mResourceBundle.getString(key);
    }
}
